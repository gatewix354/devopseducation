# DevOpsTest1


## Задание

1. Запаковать в докер код разработчика https://html5up.net/directive
2. Опубликовать в докерхаб
3. Исходный код положить в gitlab
4. Всю разработку вести в ветке dev, периодически мёржить в main по мере публикации образа в докерхаб.
5. В ветке main должны быть инструкция по запуску образа в формате Markdown

Для проверки задания прислать имя образа и ссылка на публичную репу

## Результат

### Dockerfile
```Dockerfile
FROM nginx
WORKDIR /app
COPY ./frontend /usr/share/nginx/html
```
### Сборка образа
```bash
docker build . -t html5up    
```

### Запуск образа
```bash
docker run -d -p 8080:80 html5up  
```
### Образ - https://hub.docker.com/r/artem79/html5up/tags